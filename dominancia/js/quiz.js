$(document).ready(function() {

    //AREA DAS VARIAVES GLOBAIS
    var numPerguntas = 5; // Quantas perguntas tem?
    var qtdOpcao = 5; // Quantas opcao tem cada pergunta?

    var titulo = $('.titulo').data('titulo'); //  Captura o titulo

    var perguntaAtual = 1;

	  variaveis()

     //AREA DAS AÇÕES
    $("html, body").hide();
    $("html, body").fadeIn(1500);

    atualizartexto();

    //click das opcoes
    $(".op").click(function(){
      var marcado=  $(this).data("gabarito");
      correcao(marcado,opcaocorreta);
      $(this).addClass(tipo);
      $(this).append(mensage_retorno);
      $(this).removeClass('op');
    });

    //click no botao proximo
      $(".botao-quiz").click(function(){
        if(perguntaAtual <= numPerguntas){
          proximo();
        }
        if(perguntaAtual > numPerguntas){
          fim();
        }
      });

    //AREA DAS FUNÇÕES

    //funcao de verifivar a resposta
    function correcao(marcado,opcaocorreta){
       if(marcado==opcaocorreta) {
          //funcao quando acerta
          tipo = 'classcerta';
          mensage_retorno = '<div class="remov">'+'<h3>Correto</h3>'+ msgCerta+'</div>';
       }
       else{
          //funcao quando da erro
            tipo = 'classerrada';
            mensage_retorno = '<div class="remov">'+'<h3>Errado</h3>' + msgErrada+'</div>';
       }
     };

     function proximo(){
       $('.fundo-quiz').fadeOut(0);
       perguntaAtual++;
       variaveis();
       $('.op').removeClass('classcerta');
       $('.op').removeClass('classerrada');
        $('.fundo-quiz').stop().delay(500).fadeIn(1000);
        $(".remov").hide();
       atualizartexto();
     };

     function variaveis(){
       	 pergunta = $('.pergunta'+perguntaAtual).data('pergunta');
         opcao1 = $('.pergunta'+perguntaAtual).data('opcao1');
         opcao2 = $('.pergunta'+perguntaAtual).data('opcao2');
         opcao3 = $('.pergunta'+perguntaAtual).data('opcao3');
         opcao4 = $('.pergunta'+perguntaAtual).data('opcao4');
         opcao5 = $('.pergunta'+perguntaAtual).data('opcao5');

         opcaocorreta = $('.pergunta'+perguntaAtual).data('opcaocorreta');

         msgCerta = $('.pergunta'+perguntaAtual).data('certa');
         msgErrada = $('.pergunta'+perguntaAtual).data('errada');

         botao = $('.pergunta'+perguntaAtual).data('botao');
     }

      function atualizartexto(){
        $(".titulo-quiz").html(titulo);
        $(".pergunta-quiz").html(pergunta);
        $("#p1").html(opcao1);
        $("#p2").html(opcao2);
        $("#p3").html(opcao3);
        $("#p4").html(opcao4);
        $("#p5").html(opcao5);
        $(".botao-quiz").html(botao);
      }

      function fim(){
        $('.pergunta-quiz').html("Parabéns, você concluiu a atividade.");
        $('.titulo-quiz, .respostas-quiz, .botao-quiz').hide();
      }
});
