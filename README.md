Este README normalmente documentaria quaisquer etapas necessárias para que seu aplicativo seja instalado e em execução.
O que é este repositório?

    Resumo rápido:
		Construção da transição do visual para o código.
		Hotsite construído em HTML5.
	
    Versão = 0.1 (17/04/2017),
             1.0 (19/05/2017),
             2.0 (29/05/2017).

    Learn Markdown: https://bitbucket.org/tutorials/markdowndemo.

Como faço para configurar?

    Resumo da montagem -> Sem pré requisitos, basta ter um navegador.

Diretrizes de contribuição

    Escrevendo testes -> Criar branch para teste.
    Revisão de código -> Criar branch para teste.

Com quem falo?

    Proprietário ou administrador do Repo -> Leandro Sanches <leandrosancheslima@gmail.com>
											
    Outros contatos da comunidade ou da equipe -> Equipe de Web Designer do NEaD (UNIGRANRIO).